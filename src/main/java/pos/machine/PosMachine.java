package pos.machine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class PosMachine {

    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeTolItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        String s = renderReceipt(receipt);
        return s;
    }

    public List<ReceiptItem> decodeTolItems(List<String> barcodes) {
        List<ReceiptItem> receiptItems=new ArrayList<>();
        HashMap<String, Integer> map = getBarcodeAccountHashMap(barcodes);
        for (String barcode : map.keySet()) {
            Item item = findItemByBarcode(barcode);
            ReceiptItem receiptItem=new ReceiptItem(item.getName(),map.get(barcode),item.getPrice());
            receiptItems.add(receiptItem);
        }
        return receiptItems;
    }

    private static HashMap<String, Integer> getBarcodeAccountHashMap(List<String> barcodes) {
        HashMap<String,Integer> map=new HashMap<>();
        for (String barcode : barcodes) {
            if(map.containsKey(barcode)){
                map.put(barcode,map.get(barcode)+1);
            }else{
                map.put(barcode,1);
            }
        }
        return map;
    }

    public Item findItemByBarcode(String barcode) {
        List<Item> items = ItemsLoader.loadAllItems();
        for (Item item : items) {
            if(item.getBarcode().equals(barcode)){
                return item;
            }
        }
        return null;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems){
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return new Receipt(receiptItems,totalPrice);
    }
    public String generateItemsReceipt(ReceiptItem receiptItem) {
        return "Name: "+receiptItem.getName()+", Quantity: "+receiptItem.getQuantity()+
                ", Unit price: "+receiptItem.getUnitPrice()+ " (yuan), Subtotal: "+ receiptItem.getSubTotal()+" (yuan)\n";
    }
    public String renderReceipt(Receipt receipt){
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        StringBuilder receiptString=new StringBuilder();
        receiptString.append("***<store earning no money>Receipt***\n");
        receiptItems.stream().forEach(receiptItem -> {
            receiptString.append(generateItemsReceipt(receiptItem));
        });
        receiptString.append("----------------------\n");
        receiptString.append("Total: "+receipt.getTotalPrice()+" (yuan)\n");
        receiptString.append("**********************");
        return  receiptString.toString();
    }
}
