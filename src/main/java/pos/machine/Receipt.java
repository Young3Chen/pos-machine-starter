package pos.machine;

import java.util.List;

public class Receipt {
    List<ReceiptItem> receiptItems;
    Integer totalPrice;

    public Receipt(List<ReceiptItem> receiptItems, Integer totalPrice) {
        this.receiptItems = receiptItems;
        this.totalPrice = totalPrice;
    }

    public List<ReceiptItem> getReceiptItems() {
        return receiptItems;
    }

    public void setReceiptItems(List<ReceiptItem> receiptItems) {
        this.receiptItems = receiptItems;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Receipt{" +
                "receiptItems=" + receiptItems +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
